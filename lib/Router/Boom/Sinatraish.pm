package Router::Boom::Sinatraish;
use 5.008005;
use strict;
use warnings;
use parent qw/Exporter/;
our $VERSION = '0.01';
use Router::Boom::Method;

our @EXPORT = qw/router any get post/;

sub router {
    my $class = shift;
    no strict 'refs';
    no warnings 'once';
    ${"${class}::ROUTER"} ||= Router::Boom::Method->new();
}

# any [qw/get post delete/] => '/bye' => sub { ... };
# any '/bye' => sub { ... };
sub any($$;$) {
    my $pkg = caller(0);
    if (@_==3) {
        my ($methods, $pattern, $code) = @_;
        $pkg->router->add(
            [ map { uc $_ } @$methods ],
            $pattern,
            {code => $code},
        );
    } else {
        my ($pattern, $code) = @_;
        $pkg->router->add(
            undef,
            $pattern,
            {code => $code},
        );
    }
}

sub get  {
    my $pkg = caller(0);
    $pkg->router->add(['GET', 'HEAD'], $_[0], {code => $_[1]});
}

sub post {
    my $pkg = caller(0);
    $pkg->router->add(['POST'], $_[0], {code => $_[1]});
}

1;
__END__

=encoding utf-8

=head1 NAME

Router::Boom::Sinatraish - Sinatra-ish router on Router::Boom

=head1 SYNOPSIS

    package MySinatraishFramework;
    use Router::Boom;

    sub import {
        Router::Boom->export_to_level(1);
    }

    sub to_app {
        my ($class) = caller(0);
        sub {
            my $env = shift;
            my ($route, $capt, $not_allowed) = $class->router->match($env);
            if ($now_allowed) {
                return [405, [], ['method not allowed']];
            } elsif ($route) {
                return $route->{code}->($env);
            } else {
                return [404, [], ['not found']];
            }
        };
    }

    package MyApp;
    use MySinatraishFramework;

    get '/' => sub {
        [200, [], ['ok']];
    };
    post '/edit' => sub {
        [200, [], ['ok']];
    };
    any '/any' => sub {
        [200, [], ['ok']];
    };

    __PACKAGE__->to_app;

=head1 DESCRIPTION

Router::Boom::Sinatraish is toolkit library for sinatra-ish WAF.

=head1 EXPORTABLE METHODS

=over 4

=item my $router = YourClass->router;

Returns this instance of L<Router::Boom::Method>.

=back

=head1 EXPORTABLE FUNCTIONS

=over 4

=item get($path:Str, $code:CodeRef)

    get '/' => sub { ... };

Add new route, handles GET method.

=item post($path:Str, $code:CodeRef)

    post '/' => sub { ... };

Add new route, handles POST method.

=item any($path:Str, $code:CodeRef)

    any '/' => sub { ...  };

Add new route, handles any HTTP method.

=item any($methods:ArrayRef[Str], $path:Str, $code:CodeRef)

    any [qw/GET DELETE/] => '/' => sub { ...  };

Add new route, handles any HTTP method.

=back

=head1 SEE ALSO

L<Router::Simple::Sinatraish>

L<Router::Boom>

L<Router::Boom::Method>

=head1 AUTHOR

Tokuhiro Matsuno E<lt>tokuhirom AAJKLFJEF@ GMAIL COME<gt>

ITO Nobuaki E<lt>daydream.trippers@gmail.comE<gt>

=head1 LICENSE

Copyright (C) Tokuhiro Matsuno, ITO Nobuaki.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
