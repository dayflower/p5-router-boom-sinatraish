# NAME

Router::Boom::Sinatraish - Sinatra-ish router on Router::Boom

# SYNOPSIS

    package MySinatraishFramework;
    use Router::Boom;

    sub import {
        Router::Boom->export_to_level(1);
    }

    sub to_app {
        my ($class) = caller(0);
        sub {
            my $env = shift;
            my ($route, $capt, $not_allowed) = $class->router->match($env);
            if ($now_allowed) {
                return [405, [], ['method not allowed']];
            } elsif ($route) {
                return $route->{code}->($env);
            } else {
                return [404, [], ['not found']];
            }
        };
    }

    package MyApp;
    use MySinatraishFramework;

    get '/' => sub {
        [200, [], ['ok']];
    };
    post '/edit' => sub {
        [200, [], ['ok']];
    };
    any '/any' => sub {
        [200, [], ['ok']];
    };

    __PACKAGE__->to_app;

# DESCRIPTION

Router::Boom::Sinatraish is toolkit library for sinatra-ish WAF.

# EXPORTABLE METHODS

- my $router = YourClass->router;

    Returns this instance of [Router::Boom::Method](https://metacpan.org/pod/Router::Boom::Method).

# EXPORTABLE FUNCTIONS

- get($path:Str, $code:CodeRef)

        get '/' => sub { ... };

    Add new route, handles GET method.

- post($path:Str, $code:CodeRef)

        post '/' => sub { ... };

    Add new route, handles POST method.

- any($path:Str, $code:CodeRef)

        any '/' => sub { ...  };

    Add new route, handles any HTTP method.

- any($methods:ArrayRef\[Str\], $path:Str, $code:CodeRef)

        any [qw/GET DELETE/] => '/' => sub { ...  };

    Add new route, handles any HTTP method.

# SEE ALSO

[Router::Simple::Sinatraish](https://metacpan.org/pod/Router::Simple::Sinatraish)

[Router::Boom](https://metacpan.org/pod/Router::Boom)

[Router::Boom::Method](https://metacpan.org/pod/Router::Boom::Method)

# AUTHOR

Tokuhiro Matsuno <tokuhirom AAJKLFJEF@ GMAIL COM>

ITO Nobuaki <daydream.trippers@gmail.com>

# LICENSE

Copyright (C) Tokuhiro Matsuno, ITO Nobuaki.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.
