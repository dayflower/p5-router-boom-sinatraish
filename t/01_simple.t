use strict;
use warnings;
use Test::More;

{
    package MyApp;
    use Router::Boom::Sinatraish;
    get '/' => sub {
        return "GET TOP"
    };
    post '/' => sub {
        return "POST TOP"
    };
    any '/foo' => sub {
        return "ANY FOO"
    };
    any [qw/DELETE/] => '/entry' => sub {
        return "DELETE entry"
    };
}

my $r1 = MyApp->router;
my $r2 = MyApp->router;
is "$r1", "$r2";

{
    my ($route, $capt, $not_allowed) = MyApp->router->match('GET', '/foo');
    ok $route, '/foo';
    is $route->{code}->(), "ANY FOO";
}

{
    my ($route, $capt, $not_allowed) = MyApp->router->match('GET', '/');
    ok $route, 'GET /';
    is $route->{code}->(), "GET TOP";
}

{
    my ($route, $capt, $not_allowed) = MyApp->router->match('POST', '/');
    ok $route, 'POST /';
    is $route->{code}->(), "POST TOP";
}

{
    my ($route, $capt, $not_allowed) = MyApp->router->match('DELETE', '/entry');
    ok $route, 'DELETE /entry';
    is $route->{code}->(), "DELETE entry";
}

{
    my ($route, $capt, $not_allowed) = MyApp->router->match('GET', '/entry');
    ok !$route, 'GET /entry';
    ok $not_allowed, 'GET /entry not allowed';
}

done_testing;
